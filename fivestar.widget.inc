$plugin = array(
  'name' => 'my_plugin',
  'module' => 'my_module',
  'example_callback' => array(
    'file' => 'my_plugin.extrafile.inc',
    'function' => 'my_module_my_plugin_example_callback',
  ),
);