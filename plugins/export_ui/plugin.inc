<?php

$plugin = array(
  // The name of the table as found in the schema in hook_install. This
  // must be an exportable type with the 'export' section defined.
  'schema' => 'fivestar',

  // The access permission to use. If not provided it will default to
  // 'administer site configuration'
  'access' => 'administer site configuration',

  // You can actually define large chunks of the menu system here. Nothing
  // is required here. If you leave out the values, the prefix will default
  // to admin/structure and the item will default to the plugin name.
  'menu' => array(
    'menu prefix' => 'admin/structure',
    'menu item' => 'fivestar',
    // Title of the top level menu. Note this should not be translated,
    // as the menu system will translate it.
    'menu title' => 'Fivestars', 
    // Description of the top level menu, which is usually needed for
    // menu items in an administration list. Will be translated
    // by the menu system.
    'menu description' => 'Administer fivstar tags.',
  ),

  // These are required to provide proper strings for referring to the
  // actual type of exportable. "proper" means it will appear at the
  // beginning of a sentence.
  'title singular' => t('tag'),
  'title singular proper' => t('Tag'),
  'title plural' => t('tags'),
  'title plural proper' => t('Tags'),

  // This will provide you with a form for editing the properties on your
  // exportable, with validate and submit handler.
  //
  // The item being edited will be in $form_state['item'].
  //
  // The submit handler is only responsible for moving data from
  // $form_state['values'] to $form_state['item'].
  //
  // All callbacks will accept &$form and &$form_state as arguments.
  'form' => array(
    'settings' => 'fivestar_export_ui_form',
    'validate' => 'fivestar_export_ui_form_validate',
    'submit' => 'fivestar_export_ui_form_submit',
  ),

);


function fivestar_export_ui_form(&$form, &$form_state) {

  $form['info']['tag'] = array(
    '#type' => 'textfield',
    '#title' => t('Tag Name'),
    '#required' => TRUE,
   );
   
  $form['info']['widget'] = array(
    '#tree' => FALSE,
    '#type' => 'fieldset',
    '#title' => t('Tag display'),
    '#description' => t('Choose a widget set to be used on your site. Widgets supporting custom colors can be further customized by adjusting the color scheme.'),
    '#weight' => -2,
  );
  $widget = fivestar_get_widgets();
  $widgets = module_invoke_all('fivestar_widgets');
  $classic_widgets = array();
  foreach ($widgets as $path => $name) {
    $directory = dirname($path);
    $classic_widgets[$path] = $name;
  }
  dsm($classic_widgets);
  $form['widget']['fivestar_widget'] = array(
    '#type' => 'radios',
    '#options' => $classic_widgets,
    '#attributes' => array(
      'class' => array('fivestar-widgets')
    ),
  );

  $form['#theme'] = array('fivestar_settings');
}

function fivestar_export_ui_form_validate($form_state, $form_values) {
  dsm($form_state);
  dsm($form_values);
}

function fivestar_export_ui_form_submit($form_state, $form_values) {
  dsm($form_state);
  dsm($form_values);
}