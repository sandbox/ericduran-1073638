<?php

$plugin = array(
  'name' => 'basic', // The name of the Star widgets
  'description' => "Basic fivestar with support for up to 10 stars", // Basic Description about this widgets
  'max' => 10, // Max number of stars allowed
  'css' => array(
    'default' => 'drupal.css', // default css to be include
  ),
);