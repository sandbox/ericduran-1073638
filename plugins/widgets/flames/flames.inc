<?php

$plugin = array(
  'name' => 'basic', // The name of the Star widgets
  'description' => "Basic fivestar with support for up to 10 stars", // Basic Description about this widgets
  'max' => 10, // Max number of stars allowed
  'css' => array(
    'default' => 'basic.css', // default css to be include
    'rtl' => 'basic-rtl.css', // rtl languages css
  ),
  // If there's a templates variable with a star and cancel key then this means this template can be used to generate custom stars off it.
  'templates' => array(
    'star' => 'star-template.png',
    'cancel' => 'cancel-template.png',
  )
);